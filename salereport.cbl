       IDENTIFICATION DIVISION. 
       PROGRAM-ID. SALEREPORT.
       AUTHOR. SUPHAKORN
       
       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT 100-INPUT-FILE ASSIGN TO "FOODSALE.DAT"
              ORGANIZATION IS SEQUENTIAL
              FILE STATUS IS WS-INPUT-FILE-STATUS.

       DATA DIVISION. 
       FILE SECTION. 
       FD 100-INPUT-FILE 
           BLOCK CONTAINS 0 RECORDS.
       01 100-INPUT-RECORD PIC X(80).

       WORKING-STORAGE SECTION. 
       01 WS-INPUT-FILE-STATUS PIC X(2).
           88  FILE-OK   VALUE '00'.
           88  FILE-AT-END  VALUE '10'.
       01 WS-CALCULATION.
           05 WS-INPUT-COUNT PIC 9(5).    
       

       PROCEDURE DIVISION.
       0000-MAINPROGRAM.
           PERFORM 1000-INITIAL THRU 1000-EXIT
           PERFORM 2000-PROCESS THRU 2000-EXIT UNTIL FILE-AT-END 
              OF WS-INPUT-FILE-STATUS 
           PERFORM 3000-END  THRU 3000-EXIT 


           GOBACK. 


       1000-INITIAL.
           OPEN INPUT 100-INPUT-FILE 
           IF FILE-OK 
              CONTINUE
           ELSE
              DISPLAY '***** SALEREPORT ABEND *****'
              UPON CONSOLE 
              DISPLAY '* PARA 1000-INITIAL FAIL *'
              UPON CONSOLE 
              DISPLAY '* FILE STATUS = ' WS-INPUT-FILE-STATUS 
              UPON CONSOLE
              DISPLAY '***** SALEREPORT ABEND *****'
              UPON CONSOLE 
              STOP RUN 
           END-IF
           PERFORM 8000-READ-FILE THRU 8000-EXIT  
           .


       1000-EXIT.
           EXIT.


       2000-PROCESS.
           DISPLAY 100-INPUT-RECORD 
           PERFORM 8000-READ-FILE THRU 8000-EXIT 
           .
       2000-EXIT.
           EXIT.
       
       3000-END.
           CLOSE 100-INPUT-FILE 
           IF FILE-OK 
              CONTINUE
           ELSE
              DISPLAY '***** SALEREPORT ABEND *****'
              UPON CONSOLE 
              DISPLAY '* PARA  3000-END FAIL *'
              UPON CONSOLE 
              DISPLAY '* FILE STATUS = ' WS-INPUT-FILE-STATUS 
              UPON CONSOLE
              DISPLAY '***** SALEREPORT ABEND *****'
              UPON CONSOLE 
              STOP RUN 
           END-IF
           DISPLAY 'READ ' WS-INPUT-COUNT ' RECORDS'
           .
       3000-EXIT.
           EXIT.

       8000-READ-FILE.
           READ 100-INPUT-FILE
           IF FILE-OK OF WS-INPUT-FILE-STATUS 
              ADD 1 TO WS-INPUT-COUNT
           ELSE
              IF FILE-AT-END OF WS-INPUT-FILE-STATUS 
                 CONTINUE
              ELSE
                 DISPLAY '***** SALEREPORT ABEND *****'
              UPON CONSOLE 
              DISPLAY '* PARA 8000-READ-FILE. FAIL *'
              UPON CONSOLE 
              DISPLAY '* FILE STATUS = ' WS-INPUT-FILE-STATUS 
              UPON CONSOLE
              DISPLAY '***** SALEREPORT ABEND *****'
              UPON CONSOLE 
              STOP RUN 
              END-IF    
           END-IF 
           .
       8000-EXIT.
           EXIT.